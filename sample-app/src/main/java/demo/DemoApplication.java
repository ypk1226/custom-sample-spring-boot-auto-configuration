package demo;

import com.example.learn.girlfriend.as.third.party.GirlfriendAtHome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@SpringBootApplication//(exclude = GirlfriendAutoConfiguration.class)
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Service
    static class GirlfriendLoader implements ApplicationContextAware {
        private static final Logger logger = LoggerFactory.getLogger(GirlfriendLoader.class);

        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            boolean exist = applicationContext.containsBean("girlfriendAtHome");
            if (exist) {
                GirlfriendAtHome girlfriendAtHome = applicationContext.getBean(GirlfriendAtHome.class);
                logger.info("The girl friend came, the mood is " + girlfriendAtHome.getMood());
            } else {
                logger.error("The girl friend gone away!");
            }

        }
    }
}
