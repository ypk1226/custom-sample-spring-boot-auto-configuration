package com.example.learn.girlfriend.spring.boot.autoconfigure;

public interface MoodConst {
    String HAPPY = "happy";
    String SAD = "sad";
}
