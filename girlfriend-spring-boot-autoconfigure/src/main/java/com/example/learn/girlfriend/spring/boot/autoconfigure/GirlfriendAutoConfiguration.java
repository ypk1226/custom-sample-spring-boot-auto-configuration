package com.example.learn.girlfriend.spring.boot.autoconfigure;

import com.example.learn.girlfriend.as.third.party.GirlfriendAtHome;
import com.example.learn.girlfriend.spring.boot.autoconfigure.condition.ConditionalOnMotherInLaw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

@Configuration
@ConditionalOnClass({GirlfriendAtHome.class})//必须在家
@ConditionalOnMotherInLaw(mood = MoodConst.HAPPY)//必须高兴才加载这个自动配置
@EnableConfigurationProperties(GirlfriendProperties.class)
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
public class GirlfriendAutoConfiguration {

    private final GirlfriendProperties properties;

    @Autowired
    public GirlfriendAutoConfiguration(GirlfriendProperties properties) {
        this.properties = properties;
    }

    @Bean
    public GirlfriendAtHome girlfriendAtHome() {
        String mood = properties.getMood();
        mood = mood == null ? "未知" : mood;
        GirlfriendAtHome girlfriendAtHome = new GirlfriendAtHome();
        girlfriendAtHome.setMood(mood);
        girlfriendAtHome.setName("甄姬");
        girlfriendAtHome.setAge("18");
        return girlfriendAtHome;
    }

}
