package com.example.learn.girlfriend.spring.boot.autoconfigure.condition;

import com.example.learn.girlfriend.spring.boot.autoconfigure.MoodConst;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Map;

public class OnMotherInLawCondition extends SpringBootCondition {
    private static final String DEFAULT_MOOD = MoodConst.HAPPY;//假定一直很开心

    @Override
    public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Map<String, Object> attributes = metadata.getAnnotationAttributes(ConditionalOnMotherInLaw.class.getName());
        String mood = attributes.get("mood").toString();
        boolean match = DEFAULT_MOOD.equals(mood);
        String message = match ? "心情匹配" : "心情不匹配";
        return new ConditionOutcome(match, message);
    }

}
