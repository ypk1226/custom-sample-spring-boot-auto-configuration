package com.example.learn.girlfriend.spring.boot.autoconfigure.condition;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional({OnMotherInLawCondition.class})
public @interface ConditionalOnMotherInLaw {
    String mood();//岳母心情

}
