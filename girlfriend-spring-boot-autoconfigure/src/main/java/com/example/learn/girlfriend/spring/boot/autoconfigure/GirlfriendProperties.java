package com.example.learn.girlfriend.spring.boot.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("third.party.girlfriend")
public class GirlfriendProperties {

    private String mood;//心情

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }
}