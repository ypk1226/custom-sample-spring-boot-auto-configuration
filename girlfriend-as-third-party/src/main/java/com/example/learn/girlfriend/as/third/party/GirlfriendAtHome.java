package com.example.learn.girlfriend.as.third.party;

public class GirlfriendAtHome {
	private String name;//姓名
	private String age;//年龄
	private String mood;//情绪

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getMood() {
		return mood;
	}

	public void setMood(String mood) {
		this.mood = mood;
	}
}
